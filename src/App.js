import React, { Component } from 'react';
import Drawer from './components/Drawer'
import NavBar from './components/NavBar'
import Cards from './components/Cards'
import Paper from './components/Paper'
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Drawer />
        <NavBar />
        <Cards />
        <Paper />
      </div>
    );
  }
}

export default App;
